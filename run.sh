#!/usr/bin/env bash

export HIVE_HOME=/usr/hdp/current/hive-server2/
export HCAT_HOME=/usr/hdp/current/hive-webhcat/
export FLUME_HOME=/usr/apache-flume-1.6.0-bin
$FLUME_HOME/bin/flume-ng agent --conf conf --conf-file /root/flume/kafka_flume.conf --name tier1
