package com.epam.flume;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang.StringUtils.isNotEmpty;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.interceptor.Interceptor;
import org.apache.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.flume.cassandra.BackupRepository;

/**
 * Created by atomkevich on 4/25/16.
 * 
 * Joins user tags with raw events from kafka.
 */
public class TagsInterceptor implements Interceptor {
    public static final String TS_HEADER_NAME = "ts";
    private static final Logger LOG = Logger.getLogger(TagsInterceptor.class);

    private ClassPathXmlApplicationContext context;
    private BackupRepository backupRepository;

    @Override
    public void initialize() {
        context = new ClassPathXmlApplicationContext("application-context.xml");
        backupRepository = context.getBean(BackupRepository.class);
    }

    @Override
    public Event intercept(Event event) {
        String body = new String(event.getBody());
        String[] splits = body.toString().split("\t");

        Map<String, String> headers = event.getHeaders();
        headers.put(TS_HEADER_NAME, convertToDate(splits[1]));

        String bidId = splits[0];
        String tags = backupRepository.getUserTags(bidId);
        if (isNotEmpty(tags)) {
            event.setBody((format("%s\t%s", body, tags)).getBytes());
        }

        return event;
    }

    private String convertToDate(String date) {
        Long ts = Long.valueOf(date) / 1000000000;
        return ts / 10000 + "-" + ts / 100 % 100 + "-" + ts % 100;
    }

    @Override
    public List<Event> intercept(List<Event> events) {
        return events.stream().map(e -> intercept(e)).collect(toList());
    }

    @Override
    public void close() {
        try {
            if (context != null) context.close();
            if (backupRepository != null) backupRepository.close();
        } catch (IOException e) {
            LOG.error(format("Error while closing backupRepository. Caused by: %s", e.getCause()));
        }
    }

    public static class Builder implements Interceptor.Builder {
        @Override
        public Interceptor build() {
            return new TagsInterceptor();
        }

        @Override
        public void configure(Context context) {}
    }

    // For testing purpose only
    public BackupRepository getBackupRepository() {
        return backupRepository;
    }

    public void setBackupRepository(BackupRepository backupRepository) {
        this.backupRepository = backupRepository;
    }

    public ClassPathXmlApplicationContext getContext() {
        return context;
    }

    public void setContext(ClassPathXmlApplicationContext context) {
        this.context = context;
    }
}
