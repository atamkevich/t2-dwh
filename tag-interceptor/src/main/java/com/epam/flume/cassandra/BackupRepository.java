package com.epam.flume.cassandra;

import static com.google.common.collect.Maps.newConcurrentMap;

import java.io.Closeable;
import java.io.IOException;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

/**
 * @author mikalai_kautur@epam.com
 *
 * TODO: May be anyone will also create repositoris for cassandra? Then suppose, we need to merge this into shared module.
 */
@Repository
public class BackupRepository implements Closeable {
    private static final String GET_USER_TAGS_BY_BID_ID = "SELECT tags FROM bidding.backup WHERE bid_id = ?";
    private volatile Map<String, PreparedStatement> cache = newConcurrentMap();

    @Autowired private CassandraConnector connector;
    private Session session;

    @PostConstruct
    public void init() {
        session = connector.getSession();
    }

    public String getUserTags(String bidId) {
        Row row =  execute(GET_USER_TAGS_BY_BID_ID, bidId).one();
        return row == null ? null : row.getString(0);
    }

    private ResultSet execute(String query, Object... values) {
        PreparedStatement st = cache.get(query);
        // DCL 
        if (st == null) {
            synchronized (cache) {
                if (cache.get(query) == null) {
                    st = session.prepare(query);
                    cache.put(query, st);
                }
            }
        }
        BoundStatement boundSt = values.length == 0 ? st.bind() : st.bind(values);
        return session.execute(boundSt);
    }

    public void close() throws IOException {
        connector.close();
    }

    public Session getSession() {
        return session;
    }
}
