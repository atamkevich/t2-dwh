package com.epam.flume.cassandra;

import org.springframework.stereotype.Component;

/**
 * @author mikalai_kautur@epam.com
 */
@Component
public class CassandraConfig {
    private String host;
    private String port;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }
}
